# Frontend learning pathway tracker and exercise files

This repo contains my solution files for MDN's Frontend Learning Path. The original repo is [here](https://github.com/mdn/learning-area/) and the learning material is [here](https://developer.mozilla.org/en-US/Learn).
